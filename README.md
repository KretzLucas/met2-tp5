# MET2TP5

Pour faire fonctionner le projet il est nécessaire de :
* Lancer le serveur de l'api présente dans le dossier BACK ou à l'adresse suivante : https://codenvy.io/dashboard/#/ide/LKRETZ/MET2
* Aller dans le dossier : FRONT/src/environments
* Ouvrir le fichier : environments.ts
* Modifier les trois liens des variables : listeAlbums, listeUtilisateurs, login par le lien créé par le serveur en gardant le contenu après le dernier /
* Lancer l'application angular avec le proxy via la commande suivante : ng serve --proxy-config proxy.conf.json