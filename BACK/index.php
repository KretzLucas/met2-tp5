<?php 
require '../vendor/autoload.php';
$app = new \Slim\App;

use \Firebase\JWT\JWT;

const JWT_SECRET = "MaCle6768";

$jwt = new \Slim\Middleware\JwtAuthentication([
    "path" => "/api",
    "secure" => false,
    "secret" => JWT_SECRET,
    "passthrough" => ["/login"],
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {
        $data = array('ERREUR' => 'ERREUR', 'ERREUR' => 'AUTO');
        return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }
]);

$app->add($jwt);

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization, X-Requested-With');
header('Access-Control-Expose-Headers: Authorization');
header('content-type: text/plain');  
header("content-type: application/x-www-form-urlencoded");

$app->get('/clients', 'getClients');
$app->get('/clients/{id}', 'getClient');
$app->post('/clients', 'addClient');
$app->put('/clients/{id}', 'updateClient');
$app->delete('/clients/{id}', 'deleteClient');

$app->get('/produits', 'getProduits');
$app->get('/produits/{id}', 'getProduit');
$app->post('/produits', 'addProduit');
$app->put('/produits/{id}', 'updateProduit');
$app->delete('/produits/{id}', 'deleteProduit');

$app->post('/login', 'login');


//---------- Début partie client ----------

function getClients($request, $response, $args) {
    $json = file_get_contents('./utilisateur.json');
    $data = json_decode($json);
    
    return $response->write(json_encode($data));
}

function getClient($request, $response, $args) {
    $id = $args['id'];
    $json = file_get_contents('./utilisateur.json');
    $data = json_decode($json);
    
    $client = null;
    $i = 0;
    $arret = false;
    
    while($i < count($data) && !$arret) {
        if($data[$i]->id == $id) {
            $client = $data[$i];
            $arret = true;
        }
        $i++;
    }
    
    
    return $response->write(json_encode($client));
}

function addClient($request, $response, $args) {
    $body = $request->getParsedBody();
    $ajout = false;
    
    if(!ClientExist($body['id'])) {
        $json = file_get_contents('./utilisateur.json');
        $data = json_decode($json, true);
        $data[] = $body;
        $newJson = json_encode($data);
        file_put_contents('./utilisateur.json', $newJson);
        $ajout = true;
    }
        
    return $response->write($ajout);
}

function updateClient($request, $response, $args) {
    $id = $args['id'];
    $miseAJour = false;
    
    if(ClientExist($id)) {
        $body = $request->getParsedBody();   
        $json = file_get_contents('./utilisateur.json');
        $data = json_decode($json);
        
        $i = 0;
        $arret = false;
    
        while($i < count($data) && !$arret) {
            if($data[$i]->id == $id) {
                $data[$i] = $body;
                $arret = true;
            }
            $i++;
        }
        
        $newJson = json_encode($data);
        file_put_contents('./utilisateur.json', $newJson);
        $miseAJour = true;
    }
    
return $response->write($miseAJour);
}

function deleteClient($request, $response, $args) {
    $id = $args['id'];
    $suppression = false;
    
    if(ClientExist($id)) {
        $json = file_get_contents('./utilisateur.json');
        $data = json_decode($json);
        
        $i = 0;
        $arret = false;
        
        while($i < count($data) && !$arret) {
            if($data[$i]->id == $id) {
                array_splice($data, $i, 1);
                $arret = true;
            }
            $i++;
        }
        
        $newJson = json_encode($data);
        file_put_contents('./utilisateur.json', $newJson);
        $suppression = true;
    }
    
    return $response->write($suppression);
}

function ClientExist($id) {
    $json = file_get_contents('./utilisateur.json');
    $data = json_decode($json);
    
    $i = 0;
    $exist = false;
    
    while($i < count($data) && !$exist) {
        if($data[$i]->id == $id) {
            $exist = true;
        }
        $i++;
    }
    
    return $exist;
}

//---------- Fin partie client ----------

//---------- Début partie produit ----------

function getProduits($request, $response, $args) {
    $header = $request->getHeader("Authorization");
    $tokenArray = str_replace('Bearer ', '', $header);
    $token = $tokenArray[0];
    $decode = JWT::decode($token, JWT_SECRET, array('HS256'));
    
    $json = file_get_contents('./album.json');
    $data = json_decode($json);
    
    return $response->write(json_encode($data));
}

function getProduit($request, $response, $args) {    
    $id = $args['id'];
    $json = file_get_contents('./album.json');
    $data = json_decode($json);
    
    $produit = null;
    $i = 0;
    $arret = false;
    
    while($i < count($data) && !$arret) {
        if($data[$i]->id == $id) {
            $produit = $data[$i];
            $arret = true;
        }
        $i++;
    }
    
    
    return $response->write(json_encode($produit));
}

function addProduit($request, $response, $args) {
    $body = $request->getParsedBody();
    $ajout = false;
    
    if(!ProduitExist($body['id'])) {
        $json = file_get_contents('./album.json');
        $data = json_decode($json, true);
        $data[] = $body;
        $newJson = json_encode($data);
        file_put_contents('./album.json', $newJson);
        $ajout = true;
    }
        
    return $response->write($ajout);
}

function updateProduit($request, $response, $args) {
    $id = $args['id'];
    $miseAJour = false;
    
    if(ProduitExist($id)) {
        $body = $request->getParsedBody();   
        $json = file_get_contents('./album.json');
        $data = json_decode($json);
        
        $i = 0;
        $arret = false;
    
        while($i < count($data) && !$arret) {
            if($data[$i]->id == $id) {
                $data[$i] = $body;
                $arret = true;
            }
            $i++;
        }
        
        $newJson = json_encode($data);
        file_put_contents('./album.json', $newJson);
        $miseAJour = true;
    }
    
return $response->write($miseAJour);
}

function deleteProduit($request, $response, $args) {
    $id = $args['id'];
    $suppression = false;
    
    if(ProduitExist($id)) {
        $json = file_get_contents('./album.json');
        $data = json_decode($json);
        
        $i = 0;
        $arret = false;
        
        while($i < count($data) && !$arret) {
            if($data[$i]->id == $id) {
                array_splice($data, $i, 1);
                $arret = true;
            }
            $i++;
        }
        
        $newJson = json_encode($data);
        file_put_contents('./album.json', $newJson);
        $suppression = true;
    }
    
    return $response->write($suppression);
}

function ProduitExist($id) {
    $json = file_get_contents('./album.json');
    $data = json_decode($json);
    
    $i = 0;
    $exist = false;
    
    while($i < count($data) && !$exist) {
        if($data[$i]->id == $id) {
            $exist = true;
        }
        $i++;
    }
    
    return $exist;
}

//---------- Fin partie produit ----------

//---------- Début partie JWT ----------

function login ($request, $response, $args) {
    $body = $request->getParsedBody();
    
    $userLogin = $body['nomDeCompte'];
    $userPwd = $body['motDePasse'];

    $json = file_get_contents('./utilisateur.json');
    $data = json_decode($json);
    
    $i = 0;
    $exist = false;
    
    while($i < count($data) && !$exist) {
        if($data[$i]->nomDeCompte == $userLogin) {
            if($data[$i]->motDePasse == $userPwd) {
                $exist = true;
            }
        }
        $i++;
    }

    if (!$exist) {
        return $response->withJson([
            'success' => false,
            'message' => "Username or Password false"
        ]);
    }
    
    $issuedAt = time();
    $expirationTime = $issuedAt + 60; // jwt valid for 60 seconds from the issued time
    $payload = array(
        'userid' => $userLogin,
        'iat' => $issuedAt,
        'exp' => $expirationTime
    );
    
    $token_jwt = JWT::encode($payload, JWT_SECRET, "HS256");
    
    $response = $response->withHeader("Authorization", "Bearer {$token_jwt}")->withHeader("Content-Type", "application/json");
    
    return $response->withHeader("Content-Type", "application/json")->withJson([
        'success' => true,
        'message' => "Login Successfull",
    ]);
}

//---------- Fin partie JWT ----------

$app->run(); 
?>