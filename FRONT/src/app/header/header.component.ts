import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  nbrAlbums: number = 0;
  constructor(private store: Store) {
    this.store.select(state => state.album.album).subscribe(u => this.nbrAlbums = u.length);
   }

  ngOnInit() {
  }

  PanierRempli() {
    let res: boolean = false;

    if(this.nbrAlbums > 0) {
      res = true;
    }

    return res;
  }
}
