import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { FormulaireRoutingModule } from './formulaire-routing.module';
import { FormulaireComponent } from './formulaire.component';
import { RecapitulatifComponent } from './recapitulatif/recapitulatif.component';
import { TelephonePipe } from '../telephone.pipe';
import { ErrorDirective } from '../error.directive';

@NgModule({
  declarations: [FormulaireComponent, RecapitulatifComponent,
    TelephonePipe,
    ErrorDirective,],
  imports: [
    CommonModule,
    FormulaireRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [TelephonePipe]
})
export class FormulaireModule { }
