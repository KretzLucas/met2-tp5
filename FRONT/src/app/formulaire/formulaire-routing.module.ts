import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormulaireComponent } from './formulaire.component';
import { RecapitulatifComponent } from './recapitulatif/recapitulatif.component';


const routes: Routes = [
  {path:'', component:FormulaireComponent},
  {path:'Recapitulatif', component:RecapitulatifComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormulaireRoutingModule { }
