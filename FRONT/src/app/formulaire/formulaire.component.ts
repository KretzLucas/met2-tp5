import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store'

import { TelephonePipe } from '../telephone.pipe';
import { AddUtilisateur } from '../model/utilisateur/utilisateur.action.add'
import { Utilisateur } from '../model/utilisateur/utilisateur';
import { EchangeService } from '../echange.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.sass']
}) 
export class FormulaireComponent implements OnInit {
    inscriptionForm: FormGroup;
    utilisateur = new Utilisateur();
    utilisateurs: Array<Utilisateur> = new Array();

    constructor(private formBuilder: FormBuilder, private telephonePipe: TelephonePipe, private router: Router, private store: Store, private echangeService: EchangeService, private http: HttpClient) {
        this.inscriptionForm = this.formBuilder.group({
            inputNom: ['', [Validators.required, Validators.pattern("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$")]],
            inputPrenom: ['', [Validators.required, Validators.pattern("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$")]],
            inputCivilite: ['', [Validators.required, Validators.minLength(1)]],
            inputAdresse: ['', [Validators.required, Validators.minLength(1)]],
            inputVille: ['', [Validators.required, Validators.minLength(1)]],
            inputCodePostal: ['', [Validators.required, Validators.pattern("^(([0-8][0-9])|(9[0-5]))[0-9]{3}$")]],
            inputEmail: ['', [Validators.required, Validators.pattern("[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})")]],
            inputTelephone: ['', [Validators.required, Validators.pattern("^(0|\\+33)[1-9]([-. ]?[0-9]{2}){4}$")]],
            inputNomDeCompte: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9_]{3,16}")]],
            inputMotDePasse: ['', [Validators.required, Validators.minLength(8)]],
            inputConfirmation: ['', [Validators.required, Validators.minLength(8)]]
        });
    }

    ngOnInit() {
        this.echangeService.GetUtilisateur().subscribe(utilisateur => this.utilisateurs = utilisateur);
    }

    TestNom(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputNom.valid) {
            valide = true;
        }

        return valide;
    }

    TestPrenom(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputPrenom.valid) {
            valide = true;
        }

        return valide;
    }

    TestCivilite(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputCivilite.valid) {
            valide = true;
        }

        return valide;
    }

    TestAdresse(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputAdresse.valid) {
            valide = true;
        }

        return valide;
    }

    TestVille(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputVille.valid) {
            valide = true;
        }

        return valide;
    }

    TestCodePostal(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputCodePostal.valid) {
            valide = true;
        }

        return valide;
    }

    TestEmail(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputEmail.valid) {
            valide = true;
        }

        return valide;
    }

    TestTelephone(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputTelephone.valid) {
            valide = true;
        }

        return valide;
    }

    TestNomDeCompte(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputNomDeCompte.valid) {
            valide = true;
        }

        return valide;
    }

    TestMotDePasse(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputMotDePasse.valid) {
            valide = true;
        }

        return valide;
    }

    TestConfirmation(): boolean {
        let valide: boolean = false;

        if (this.inscriptionForm.controls.inputConfirmation.value == this.inscriptionForm.controls.inputMotDePasse.value) {
            valide = true;
        }

        return valide;
    }

    onSubmit() {
        if (this.TestConfirmation()) {
            let i: number = 0;
            let trouve: boolean = false;

            while(!trouve && i < this.utilisateurs.length) {
                if(this.utilisateurs[i].nomDeCompte == this.inscriptionForm.controls.inputNomDeCompte.value || this.utilisateurs[i].email == this.inscriptionForm.controls.inputEmail.value) {
                    trouve = true;
                    alert("Nom de compte ou email déjà existant, veuillez en choisir un(e) autre.")
                }
                
                i++;
            }

            if(!trouve) {
                let j: number = 0;
                let id: number = this.utilisateurs[0].id;

                while(j < this.utilisateurs.length) {
                    if(id < this.utilisateurs[j].id) {
                        id = this.utilisateurs[j].id;
                    }
                    
                    j++;
                }

                id++;

                this.utilisateur.id = id;
                this.utilisateur.nom = this.inscriptionForm.controls.inputNom.value;
                this.utilisateur.prenom = this.inscriptionForm.controls.inputPrenom.value;
                this.utilisateur.civilite = this.inscriptionForm.controls.inputCivilite.value;
                this.utilisateur.adresse = this.inscriptionForm.controls.inputAdresse.value;
                this.utilisateur.ville = this.inscriptionForm.controls.inputVille.value;
                this.utilisateur.codePostal = this.inscriptionForm.controls.inputCodePostal.value;
                this.utilisateur.email = this.inscriptionForm.controls.inputEmail.value;
                this.utilisateur.telephone = this.telephonePipe.transform(this.inscriptionForm.controls.inputTelephone.value);
                this.utilisateur.nomDeCompte = this.inscriptionForm.controls.inputNomDeCompte.value;
                this.utilisateur.motDePasse = this.inscriptionForm.controls.inputMotDePasse.value;
                this.utilisateur.confirmation = this.inscriptionForm.controls.inputConfirmation.value;

                this.AddUtilisateurAPI(this.utilisateur);
                this.AddUtilisateur(this.utilisateur);
                this.router.navigate(['/Inscription/Recapitulatif']);
            }
        }
    }

    AddUtilisateur(utilisateur) { this.store.dispatch(new AddUtilisateur(utilisateur)); }

    AddUtilisateurAPI(utilisateur: Utilisateur) {
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        return this.http.post<Utilisateur>(environment.listeUtilisateurs, utilisateur, {headers : headers}).subscribe(data  => {console.log("POST Request is successful ", data);}, error  => {console.log("Error", error);});
    }
}
