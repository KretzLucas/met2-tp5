import { Component, OnInit, Input } from '@angular/core';
import { Utilisateur } from '../../model/utilisateur/utilisateur';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-recapitulatif',
  templateUrl: './recapitulatif.component.html',
  styleUrls: ['./recapitulatif.component.sass']
})
export class RecapitulatifComponent implements OnInit {
  utilisateurs: Observable<Utilisateur>;

    constructor(private store: Store) {
      this.utilisateurs = this.store.select(state => state.utilisateur.utilisateur);
    }

    ngOnInit() {

    }
}
