import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[appError]'
})
export class ErrorDirective {

   @Input() appError: boolean = false;

    constructor(private element: ElementRef) { }

    ngOnChanges() {
        if (!this.appError) {
            this.element.nativeElement.style.borderColor = 'red';
        }
        else {
            this.element.nativeElement.style.borderColor = '';
        }
    }
}
