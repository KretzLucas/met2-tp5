import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Album } from './model/album/album';
import { Utilisateur } from './model/utilisateur/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class EchangeService {
  token: any;

  httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json'})
  }

  constructor(private http: HttpClient) { }

  public GetAlbum(): Observable<Album[]> {
    this.setJWT();
    return this.http.get<Album[]>(environment.listeAlbums, this.httpOptions);
  }

  public GetUtilisateur(): Observable<Utilisateur[]> {
    return this.http.get<Utilisateur[]>(environment.listeUtilisateurs);
  }

  public setJWT () {
    this.httpOptions.headers.append("Authorization", `Bearer ${this.token}`);
  }
}

