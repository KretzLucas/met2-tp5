import { Component, OnInit } from '@angular/core';
import { EchangeService } from '../echange.service';
import { Utilisateur } from '../model/utilisateur/utilisateur';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.sass']
})
export class ConnexionComponent implements OnInit {
  inputNomDeCompte: string;
  inputMotDePasse: string;
  utilisateurs: Array<Utilisateur> = new Array();

  constructor(private echangeService: EchangeService, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.echangeService.GetUtilisateur().subscribe(utilisateur => this.utilisateurs = utilisateur);
  }

  onSubmit() {
      let i: number = 0;
      let trouve: boolean = false;

      while(!trouve && i < this.utilisateurs.length) {
        if(this.utilisateurs[i].nomDeCompte == this.inputNomDeCompte) {
          if(this.utilisateurs[i].motDePasse == this.inputMotDePasse) {
            trouve = true;
            this.echangeService.setJWT();
            this.http.post(environment.login, JSON.stringify({ nomDeCompte: this.inputNomDeCompte , motDePasse: this.inputMotDePasse }), this.echangeService.httpOptions).subscribe(data => {console.log("POST Request is successful ", data);}, error  => {console.log("Error", error);});
            alert("Connexion réussie !");
            this.router.navigate(['/Catalogue']);
          }
        }
        
        i++;
      }

      if(!trouve) {
        alert("Connexion échouée, le nom de compte ou le mot de passe est incorrect.");
      }
  }

}
