import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ConnexionRoutingModule } from './connexion-routing.module';
import { ConnexionComponent } from './connexion.component';


@NgModule({
  declarations: [ConnexionComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConnexionRoutingModule
  ]
})
export class ConnexionModule { }
