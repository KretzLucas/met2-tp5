import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store'
import { EchangeService } from '../../../echange.service';
import { Album } from '../../../model/album/album';
import { Observable } from 'rxjs';
import { AddAlbum } from '../../../model/album/album.action.add'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.sass']
})
export class DetailComponent implements OnInit {
  albums: Observable<Album[]>;

  constructor(private route: ActivatedRoute, private echangeService: EchangeService, private store: Store) { }

  ngOnInit() {
    this.albums = this.echangeService.GetAlbum();
  }

  BonId(id: number) {
    let res: boolean = false;

    if(id == +this.route.snapshot.paramMap.get('id'))
    {
      res = true;
    }

    return res;
  }

  AjouterAuPanier(album: Album) {
    this.AddAlbum(album);
  }

  AddAlbum(album) { 
    this.store.dispatch(new AddAlbum(album)); 
  }
}
