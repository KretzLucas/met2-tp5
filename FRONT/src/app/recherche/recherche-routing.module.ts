import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RechercheComponent } from './recherche.component';
import { DetailComponent } from './produit/detail/detail.component';


const routes: Routes = [
  {path:'', component:RechercheComponent},
  {path:'Catalogue/Detail/:id', component:DetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RechercheRoutingModule { }
