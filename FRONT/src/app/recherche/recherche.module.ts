import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { RechercheRoutingModule } from './recherche-routing.module';
import { RechercheComponent } from './recherche.component';
import { ProduitComponent } from './produit/produit.component';
import { FiltreAlbumPipe } from '../filtre-album.pipe';
import { DetailComponent } from './produit/detail/detail.component'

@NgModule({
  declarations: [RechercheComponent,
    ProduitComponent,
    FiltreAlbumPipe,
    DetailComponent],
  imports: [
    CommonModule,
    RechercheRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class RechercheModule { }
