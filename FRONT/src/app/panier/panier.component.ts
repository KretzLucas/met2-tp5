import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store'
import { Observable } from 'rxjs';
import { Album } from '../model/album/album';
import { DelAlbum } from '../model/album/album.action.del'

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.sass']
})
export class PanierComponent implements OnInit {
  albums: Observable<Album>;

  constructor(private store: Store) { 
    this.albums = this.store.select(state => state.album.album);
  }

  ngOnInit() {
  }

  EnleverDuPanier(album: Album) {
    this.DelAlbum(album);
  }

  DelAlbum(album) { 
    this.store.dispatch(new DelAlbum(album)); 
  }
}
