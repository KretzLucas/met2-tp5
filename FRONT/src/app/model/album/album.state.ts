import { AddAlbum } from './album.action.add'
import { DelAlbum } from './album.action.del'
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { AlbumStateModel } from './album.state.model'

@State<AlbumStateModel>({
    name: 'album',
    defaults: {
        album: []
    }
})

export class AlbumState {
    id: number = 0;

    @Selector()
        static GetAlbum(state: AlbumStateModel) {
            return state.album;
        }

    @Action(AddAlbum)
        Add({ getState, patchState }: StateContext<AlbumStateModel>, { payload }: AddAlbum) {
            const state = getState();
            patchState({
                album: [...state.album, {...payload, id: this.id}]
            });
            this.id++;
        }

    @Action(DelAlbum)
        Del({ getState, patchState }: StateContext<AlbumStateModel>, { payload }: DelAlbum) {
            const state = getState();
            patchState({
                album: [...(state.album.slice(0, state.album.indexOf(payload))), ...(state.album.slice(state.album.indexOf(payload) + 1))]
            });
        }
}
