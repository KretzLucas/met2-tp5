import { Album } from './album'

export class AddAlbum {
    static readonly type = '[Album] Add';

    constructor(public payload: Album) {

    }
}

