import { Album } from './album'

export class DelAlbum {
    static readonly type = '[Album] Del';

    constructor(public payload: Album) {

    }
}