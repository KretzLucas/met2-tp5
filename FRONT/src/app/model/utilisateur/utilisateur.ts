export class Utilisateur {
    id: number;
    nom: string;
    prenom: string;
    civilite: string;
    adresse: string;
    ville: string;
    codePostal: string;
    email: string;
    telephone: string;
    nomDeCompte: string;
    motDePasse: string;
    confirmation: string;

    constructor() {
    }
}
