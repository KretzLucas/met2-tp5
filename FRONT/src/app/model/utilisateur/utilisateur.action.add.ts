import { Utilisateur } from './utilisateur'

export class AddUtilisateur {
    static readonly type = '[Utilisateur] Add';

    constructor(public payload: Utilisateur) {

    }
}